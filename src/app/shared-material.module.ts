import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
   MatButtonModule,
   MatIconModule,
   MatFormFieldModule,
   MatInputModule,
   MatSelectModule,
   MatDatepickerModule,
   MatNativeDateModule,
   MatSnackBarModule,
   MatTooltipModule,
   MatProgressBarModule,
   MatDialogModule
} from '@angular/material';

@NgModule({
   imports: [
      CommonModule,
      MatButtonModule,
      MatIconModule,
      MatFormFieldModule,
      MatInputModule,
      MatSelectModule,
      MatDatepickerModule,
      MatNativeDateModule,
      MatSnackBarModule,
      MatTooltipModule,
      MatProgressBarModule,
      MatDialogModule
   ],
   exports: [
      MatButtonModule,
      MatIconModule,
      MatInputModule,
      MatFormFieldModule,
      MatSelectModule,
      MatDatepickerModule,
      MatNativeDateModule,
      MatSnackBarModule,
      MatTooltipModule,
      MatProgressBarModule,
      MatDialogModule
   ],
   providers: [
      MatDatepickerModule,
   ]
})

export class SharedMaterialModule { }