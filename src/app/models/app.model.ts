export class Funds {
    schemeTypes: string[];

    constructor(jsonObj) {
        if (jsonObj) {
            let modifiedData = jsonObj.split(/\n$/m);  // split data row wise
            modifiedData = modifiedData.filter(item => item); // removal of null rows
            const groups = [];
            const data = [];

            modifiedData.forEach((item) => {
                if (item != '' && item != '\n') {
                    groups.push([...item.split(/\;|\n/)]);  // separated data if ; or \n character found
                }
            });
            const dataHeadings = [...groups[0]];    // first row column headings separation
            groups.splice(0, 1);

            this.schemeTypes = [];

            groups.forEach((itemData) => {
                if (itemData && itemData.length == 2) {
                    this.schemeTypes.push(itemData[1]);     // separated rows with having scheme types
                } else if (itemData && itemData.length > 2) {
                    data.push(itemData);
                }
            });

            data.forEach((item, index) => {
                item.splice(0, 2);                  // removed first do unneccessary items
            });

            if (data && data.length && this.schemeTypes && this.schemeTypes.length) {
                data.forEach((record, index) => {
                    this[this.schemeTypes[index].trim()] = new FundRecord(record, dataHeadings); // created dynamic key as per scheme type and stored data
                    if (this[this.schemeTypes[index].trim()] && this[this.schemeTypes[index].trim()].records && this[this.schemeTypes[index].trim()].records.length) {
                        this[this.schemeTypes[index].trim()]['schemeNames'] = [];
                        this[this.schemeTypes[index].trim()].records.forEach((item) => {
                            const temp = {
                                key: item['Scheme Code'],
                                value: item['Scheme Name']
                            };

                            this[this.schemeTypes[index].trim()].schemeNames.filter(a => {
                                return a.key == item['Scheme Code'] && a.value == item['Scheme Name'];
                            }).length == 0 ? this[this.schemeTypes[index].trim()].schemeNames.push(temp) : null;   // storing distinct scheme names from all the records
                        });
                    }
                });
            }
        }
    }
}

export class FundRecord {
    records = [];

    constructor(fundData, columnsHeadings) {
        if (fundData) {
            let index = 0;
            while (index < fundData.length) {
                let tempRecord = {};
                const tempData = fundData.slice(index, 8 + index);
                columnsHeadings.forEach((element, i) => {
                    tempRecord[element.trim()] = tempData[i];      // record column headings and storing data
                });
                this.records.push(tempRecord);
                index += 8;
            }
        }
    }
}

export class ChartData {
    records: ChartRecord[];

    constructor(jsonObj) {
        this.records = [];
        if (jsonObj && jsonObj.length) {
            jsonObj.forEach(record => {
                this.records.push(new ChartRecord(record));
            });
        }
    }
}
export class ChartRecord {
    name: string;
    series: any[];

    constructor(jsonObj) {
        if (jsonObj) {
            this.name = jsonObj.name ? jsonObj.name : '';
            this.series = [];
            if (jsonObj.series && jsonObj.series.length) {
                jsonObj.series.forEach(item => {
                    this.series.push(new SeriesItem(item));
                });
            }
        } else {
            this.name = '';
            this.series = [];
        }
    }
}
export class SeriesItem {
    name: string;
    value: string;

    constructor(jsonObj) {
        if (jsonObj) {
            this.name = jsonObj['Date'] ? jsonObj['Date'] : '';
            this.value = jsonObj['Net Asset Value'] ? jsonObj['Net Asset Value'] : '';
        } else {
            this.name = '';
            this.value = '';
        }
    }
}

