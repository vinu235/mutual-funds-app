import { Pipe, PipeTransform } from '@angular/core';

import { AppService } from '../services/app.service';

@Pipe({ name: 'fundName' })
export class FundNamePipe implements PipeTransform {

    constructor(private appService: AppService) {
    }

    transform(value: string): string {
        let fundName = '';
        this.appService.fundList.forEach(item => {
            if (item.key == value) {
                fundName = item.value;
            }
        }, this);
        return fundName;
    }
}