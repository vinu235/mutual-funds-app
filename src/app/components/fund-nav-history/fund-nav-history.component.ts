import { Component, OnInit, Input, Output, EventEmitter, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Subject, Subscription } from 'rxjs';

import { AppService } from '../../services/app.service';
import { DateService } from '../../services/date.service';

@Component({
    selector: 'app-fund-nav-history',
    templateUrl: './fund-nav-history.component.html',
    styleUrls: ['./fund-nav-history.component.css']
})
export class FundNavHistoryDialog implements OnInit {
    dataRefresh: Subject<any> = new Subject();
    subscriptions: Subscription[];

    filterChartGroup: FormGroup;
    minDate: Date;
    maxDate: Date;
    toMinDate: Date;
    toMaxDate: Date;
    isFilterEnable: boolean;

    view: any[];
    showLabels: boolean;
    animations: boolean;
    xAxis: boolean;
    yAxis: boolean;
    showYAxisLabel: boolean;
    showXAxisLabel: boolean;
    timeline: boolean;
    xAxisLabel: string;
    yAxisLabel: string;
    dataLoader: boolean;
    colorScheme: any;
    results: any;

    constructor(public appService: AppService, private dateService: DateService, private fb: FormBuilder, public dialogRef: MatDialogRef<FundNavHistoryDialog>, @Inject(MAT_DIALOG_DATA) public data: any) {

        this.dialogRef.disableClose = true;
        this.minDate = new Date(2015, 3, 1);
        this.maxDate = this.dateService.addMonths(new Date(), -1);
        this.toMinDate = new Date();
        this.toMaxDate = new Date();
        this.isFilterEnable = false;
        this.subscriptions = [];

        this.createForm();
    }

    ngOnInit() {
        this.setChartProperties();
        this.filerData(true);
    }

    createForm() {
        this.filterChartGroup = this.fb.group({
            fromDate: [''],
            toDate: [{ value: '', disabled: true }]
        });
        let localsub = this.filterChartGroup.get('fromDate').valueChanges.subscribe(value => {
            if (value) {
                const minDate = this.dateService.addMonths(new Date(this.filterChartGroup.get('fromDate').value), 1);
                this.toMinDate = minDate;
                this.filterChartGroup.get('toDate').enable();
                this.filterChartGroup.get('toDate').reset();
            }
        });
        this.subscriptions.push(localsub);

        localsub = this.filterChartGroup.get('toDate').valueChanges.subscribe(value => {
            if (value) {
                this.isFilterEnable = true;
            } else {
                this.isFilterEnable = false;
            }
        });
        this.subscriptions.push(localsub);
    }

    setChartProperties() {
        this.results = this.data;
        this.view = [800, 350];
        this.showLabels = true;
        this.animations = true;
        this.xAxis = true;
        this.yAxis = true;
        this.showYAxisLabel = true;
        this.showXAxisLabel = true;
        this.timeline = true;
        this.xAxisLabel = 'Date';
        this.yAxisLabel = 'NAV';
        this.colorScheme = {
            domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
        };
    }

    filerData(initialLoad) {
        let fromDate = this.dateService.addMonths(new Date(), -3);
        let toDate = this.dateService.addMonths(new Date(), 0);

        if (!initialLoad) {
            fromDate = new Date(this.filterChartGroup.get('fromDate').value);
            toDate = new Date(this.filterChartGroup.get('toDate').value);
        }

        const fundHistory = this.data[0].series.filter(item => {
            return (new Date(item.name.trim()) >= fromDate && new Date(item.name.trim()) <= toDate) ? item : null;
        });

        const tempObj = [{
            name: this.results[0].name,
            series: fundHistory
        }];

        this.results = [...tempObj];
        this.dataRefresh.next(true);
    }

    clearFilter() {
        this.filterChartGroup.reset();
        this.filterChartGroup.get('toDate').disable();
        this.results = [...this.data];
    }

    close() {
        this.dialogRef.close();
    }

    ngOnDestroy() {
        this.appService.unSubscribe(this.subscriptions);
    }
}
