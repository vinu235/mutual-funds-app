import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-not-found',
    templateUrl: './not-found.component.html',
    styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit, OnDestroy {
    dummyTimeout: any;

    constructor(private route: Router) { }

    ngOnInit() {
        this.dummyTimeout = setTimeout(() => {
            this.route.navigate(['']);
        }, 1000);
    }

    ngOnDestroy() {
        clearTimeout(this.dummyTimeout);
    }
}
