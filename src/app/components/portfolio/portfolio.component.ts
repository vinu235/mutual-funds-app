import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';

import { AppService } from '../../services/app.service';
import { MessagesService } from '../../services/messages.service';
import { DateService } from '../../services/date.service';

import { FundNavHistoryDialog } from '../fund-nav-history/fund-nav-history.component';

@Component({
    selector: 'app-portfolio',
    templateUrl: './portfolio.component.html',
    styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {
    subscriptions: Subscription[];
    minDate: Date;
    maxDate: Date;
    purchaseFundGroup: FormGroup;
    dataLoader: boolean;
    showHistoryChart: boolean;
    isSubmitted: boolean;
    loaderMessage: string;

    constructor(private fb: FormBuilder, public appService: AppService, public dateService: DateService, private messageService: MessagesService, private dialog: MatDialog) {
        this.minDate = new Date(2015, 3, 1);
        this.maxDate = new Date();
        this.dataLoader = false;
        this.loaderMessage = 'Loading Fund Info...';
        this.showHistoryChart = false;
        this.isSubmitted = false;
        this.subscriptions = [];
        this.createForm();
    }

    ngOnInit() {
    }

    createForm() {
        this.purchaseFundGroup = this.fb.group({
            fundName: ['', Validators.required],
            schemeType: ['', Validators.required],
            scheme: ['', Validators.required],
            schemeName: [''],
            purchaseDate: [null, Validators.required],
            amount: [null, Validators.required],
            purchasedUnits: [null],
            purchaseNAV: [null],
            currentNAV: [null],
            investmentReturns: [null]
        });

        let localsub = this.purchaseFundGroup.get('fundName').valueChanges.subscribe((value) => {
            if (value) {
                this.dataLoader = true;
                this.appService.getNavHistory(value).then((resp) => {
                    this.dataLoader = false;
                    this.purchaseFundGroup.get('schemeType').reset('');
                }, error => {
                    this.dataLoader = false;
                });
            }
        });
        this.subscriptions.push(localsub);

        localsub = this.purchaseFundGroup.get('schemeType').valueChanges.subscribe((value) => {
            if (value) {
                this.purchaseFundGroup.get('scheme').reset('');
            }
        });
        this.subscriptions.push(localsub);

        localsub = this.purchaseFundGroup.get('scheme').valueChanges.subscribe((value) => {
            if (value) {
                this.appService.getSelectedFundHistory(this.purchaseFundGroup.get('schemeType').value, value);
            }
        });
        this.subscriptions.push(localsub);
    }

    purchaseFund() {
        this.isSubmitted = true;
        if (this.purchaseFundGroup.invalid) {
            this.messageService.showMessage('Please fill all the required details', 'error');
        } else {
            const data = this.purchaseFundGroup.getRawValue();
            const selectedFund = this.appService.getPurchaseFundHistory(data.purchaseDate);

            if (selectedFund) {
                const data = this.purchaseFundGroup.getRawValue();
                this.purchaseFundGroup.get('purchasedUnits').patchValue((data.amount / selectedFund['Net Asset Value']).toFixed(2));
                this.purchaseFundGroup.get('schemeName').patchValue(selectedFund['Scheme Name']);

                const currentFund = this.appService.selectedFundHistory[this.appService.selectedFundHistory.length - 1];

                if (currentFund) {
                    this.purchaseFundGroup.get('purchaseNAV').patchValue(selectedFund['Net Asset Value']);
                    this.purchaseFundGroup.get('currentNAV').patchValue(currentFund['Net Asset Value']);

                    const updatedData = this.purchaseFundGroup.getRawValue();
                    this.appService.updatePortFolio(updatedData);
                    this.messageService.showMessage('Fund Puchased Successfully', 'success');
                    this.purchaseFundGroup.reset();
                    this.purchaseFundGroup.markAsPristine();
                    this.purchaseFundGroup.markAsUntouched();
                    // this.purchaseFundGroup.get('fundName').patchValue(53);
                    this.appService.clearData();
                }
            } else {
                this.messageService.showMessage('Fund NAV history for this date not found', 'error');
            }
        }
    }

    showHistory() {
        const dialogRef = this.dialog.open(FundNavHistoryDialog, {
            width: '75vw',
            height: '75vh',
            panelClass: 'history-dialog-container',
            data: this.appService.chartData.records
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    clearPortFolio() {
        this.appService.clearPortFolioData();
    }

    ngOnDestroy() {
        this.appService.unSubscribe(this.subscriptions);
        this.dialog.closeAll();
    }
}
