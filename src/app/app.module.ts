import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { SharedMaterialModule } from './shared-material.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { PortfolioComponent } from './components/portfolio/portfolio.component';
import { FundNavHistoryDialog } from './components/fund-nav-history/fund-nav-history.component';

import { NumberOnlyDirective } from './directives/common.directive';
import { FundNamePipe } from './pipes/common.pipes';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        HomeComponent,
        NotFoundComponent,
        PortfolioComponent,
        FundNavHistoryDialog,
        NumberOnlyDirective,
        FundNamePipe
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        SharedMaterialModule,
        ReactiveFormsModule,
        NgxChartsModule
    ],
    providers: [FundNamePipe],
    entryComponents: [FundNavHistoryDialog],
    bootstrap: [AppComponent]
})
export class AppModule { }
