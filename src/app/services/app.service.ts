import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';

import { MessagesService } from './messages.service';
import { DateService } from './date.service';

import { Funds, ChartData } from '../models/app.model';

@Injectable({
    providedIn: 'root'
})
export class AppService {
    fundList: any[];
    subscriptions: Subscription[];
    fundNavHistoryData: Funds;
    portfolioData: any;
    chartData: ChartData;

    selectedFundHistory: any;
    purchaseFundHistory: any;

    constructor(private http: HttpClient, private messageService: MessagesService, private dateService: DateService) {

        this.fundList = [{ key: 53, value: 'Axis' }, { key: 69, value: 'Mahindra' }];
        this.portfolioData = {
            totalPurchaseInvestment: 0,
            totalInvestmentReturns: 0,
            totalUnits: 0,
            records: []
        };
        this.subscriptions = [];
        this.fundNavHistoryData = new Funds(null);
        this.chartData = new ChartData(null);

        const localData = localStorage.getItem('portfolioData');
        if (localData) {
            this.getLocalStorageData();
        }
    }

    getLocalStorageData() {
        this.portfolioData = JSON.parse(localStorage.getItem('portfolioData'));
    }

    setLocalStorageData() {
        localStorage.setItem('portfolioData', JSON.stringify(this.portfolioData));
        this.getLocalStorageData();
    }

    clearPortFolioData() {
        this.portfolioData = {
            totalPurchaseInvestment: 0,
            totalInvestmentReturns: 0,
            totalUnits: 0,
            records: []
        };
        this.setLocalStorageData();
    }

    getNavHistory(mfId: number = 69, type: number = 1, fromDate: string = '01-Apr-2015') {
        const url = `http://portal.amfiindia.com/DownloadNAVHistoryReport_Po.aspx?mf=${mfId}&tp=${type}&frmdt=${fromDate}&todt=${this.dateService.currentDate}`;

        return new Promise((resolve, reject) => {
            const localSub = this.http.get(url, { responseType: 'text' }).subscribe((resp) => {
                this.fundNavHistoryData = new Funds(resp);
                resolve(resp);
            }, (error) => {
                this.messageService.showMessage('Something went wrong', 'error');
                reject(error);
            });
            this.subscriptions.push(localSub);
        });
    }

    getSelectedFundHistory(schemeType, schemeCode) {
        const schemeData = this.fundNavHistoryData[schemeType];
        this.selectedFundHistory = schemeData.records.filter(element => element['Scheme Code'] == schemeCode);
        this.chartData = this.getChartData(this.selectedFundHistory);
    }

    getPurchaseFundHistory(purchaseDate) {
        const formattedDate = ('0' + purchaseDate.getDate()).slice(-2) + '-' + this.dateService.monthsList[purchaseDate.getMonth()] + '-' + purchaseDate.getFullYear();
        this.purchaseFundHistory = this.selectedFundHistory.filter(element => element['Date'].trim() == formattedDate);
        return this.purchaseFundHistory[0];
    }

    getChartData(fundHistory) {
        let chartData = new ChartData(null);
        if (fundHistory) {
            const fundForChartData = [
                {
                    name: fundHistory[0].fundName,
                    series: fundHistory
                }
            ];
            chartData = new ChartData(fundForChartData);
        }
        return chartData;
    }

    clearData() {
        this.selectedFundHistory = null;
        this.purchaseFundHistory = null;
        this.fundNavHistoryData = new Funds(null);
        this.chartData = new ChartData(null);
    }

    updatePortFolio(updatedData) {
        updatedData.investmentReturns = (updatedData.currentNAV * updatedData.purchasedUnits).toFixed(2);
        this.portfolioData.records.push(updatedData);
        this.portfolioData.totalPurchaseInvestment += parseFloat(updatedData.amount);
        this.portfolioData.totalInvestmentReturns += parseFloat(updatedData.investmentReturns);
        this.portfolioData.totalUnits += parseFloat(updatedData.purchasedUnits);
        this.setLocalStorageData();
    }

    ngOnDestroy() {
        this.unSubscribe(this.subscriptions);
    }

    unSubscribe(subscriptions) {
        if (subscriptions && subscriptions.length) {
            subscriptions.forEach(localSub => {
                localSub.unsubscribe();
            });
        }
    }
}