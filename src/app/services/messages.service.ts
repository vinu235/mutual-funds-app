import { Injectable } from '@angular/core';

import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
    providedIn: 'root'
})
export class MessagesService {

    constructor(private _snackBar: MatSnackBar) { }

    showMessage(message: string, alertClass: string) {
        this._snackBar.open(message, 'close', {
            duration: 5000,
            panelClass: ['message-wrap', alertClass]
        });
    }
}
