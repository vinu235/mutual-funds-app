import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class DateService {
    monthsList: string[];
    currentDate: string;

    constructor() {
        this.monthsList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        this.currentDate = ('0' + new Date().getDate()).slice(-2) + '-' + this.monthsList[new Date().getMonth()] + '-' + new Date().getFullYear();
    }

    getDaysInMonth(year, month) {
        return new Date(year, month, 0).getDate()
    }

    addMonths(input, months) {
        const date = new Date(input);
        date.setDate(1);
        date.setMonth(date.getMonth() + months);
        date.setDate(Math.min(input.getDate(), this.getDaysInMonth(date.getFullYear(), date.getMonth() + 1)));
        return date;
    }
}
