# mutual-funds-app

In this app you can purchase mutual funds and also you can see the NAV history chart as well.

# Dependencies

This app is created with Angular CLI Version 7. Also for rich design experience Angular Material 7.3.7 is used. 
For displaying NAV history chart the ngx-charts 13.0.4 package is used.  

# Features

User can buy Axis Dynamic Equity Fund with selecting date & investment amount, he can then able to see all the purchased funds in the same screen.

User can able see the NAV history available for Axis fund.

# Limitations

For time being the application allows for one fund selection only and limited NAV history which will be improved in the future.